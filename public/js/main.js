require.config({
    baseUrl: "public/js",
    paths: {
    	'jQuery': '../components/bower_components/jquery-2.1.0.min/index',
        'angular': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min',
        'angular-route': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-route',
        'angularAMD': '../components/bower_components/angularAMD/index'
    },
    shim: { 'angular' : ['jQuery'], 'angularAMD': ['angular'], 'angular-route': ['angular'] },
    deps: ['app']
});