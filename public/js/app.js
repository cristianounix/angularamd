define(['angularAMD', 'angular-route'], function (angularAMD) {
    var app = angular.module('webApp', ['ngRoute']);
    app.config(function ($routeProvider) {
        $routeProvider.when("/home", angularAMD.route({
            templateUrl: 'public/views/home.html', controller: 'HomeCtrl',
            controllerUrl: 'controllers/home'
        })).when("/dashboard", angularAMD.route({
            templateUrl: 'public/views/dashboard.html', controller: 'dashboardCtrl',
            controllerUrl: 'controllers/dashboard'
        })).otherwise({redirectTo: "/home"});
    });
    angularAMD.bootstrap(app);
    return app;
});